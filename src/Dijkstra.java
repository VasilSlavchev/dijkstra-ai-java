import java.util.PriorityQueue;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

class Vertex implements Comparable<Vertex>
{
    public final String name;
    public Edge[] adjacencies;
    public double minDistance = Double.POSITIVE_INFINITY;
    public Vertex previous;
    public Vertex(String Name) { name = Name; }
    public String toString() { return name; }
    public int compareTo(Vertex other)
    {
        return Double.compare(minDistance, other.minDistance);
    }
}

// TO DO!
//__input and output data and start-end Point
//__start-end point and calculating short path and the price
//__calculate cheaper path according to type of the vehicle.
//__check if it can pass through for free or under particular budget.
//__output informacion: name of segment, type and price for passing through according the type of vehicle
/* 1.First Class roads, 2.highway and 2.other roads.
 * Prrivate and comercial vehicles.
 * Price list:
 * Private vehicles:
 * 1. First 5km are free, 2st for next 10km and 1st for each kilometer next.
 * 3. 1st for higtway
 * 3. free for everyone.
 * Comecial vehicles:
 * 1. 2st for every kilometer
 * 2. 3st for first 10km, 2st every km next, and 1st for every 10km started.
 * 3. free for everyone.
 * */


class Edge
{
    public final Vertex target;
    public final double weight;
    public final double pathtype;
    public Edge(Vertex Target, double Weight, int pathType)
    { 
    	target = Target;
    	weight = Weight;
    	pathtype = pathType; // Variable for Type of the path (1, 2, 3)
    }
}

public class Dijkstra
{
    public static void computePaths(Vertex source)
    {
        source.minDistance = 0.;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
      	vertexQueue.add(source);

	while (!vertexQueue.isEmpty()) {
	    Vertex u = vertexQueue.poll();

            // Visit each edge exiting u
            for (Edge e : u.adjacencies)
            {
                Vertex v = e.target;
                double weight = e.weight;
                double distanceThroughU = u.minDistance + weight;
                if (distanceThroughU < v.minDistance) {
			    vertexQueue.remove(v);
			    v.minDistance = distanceThroughU ;
			    v.previous = u;
			    vertexQueue.add(v);
                }
            }
        }
    }

    public static List<Vertex> getShortestPathTo(Vertex target)
    {
        List<Vertex> path = new ArrayList<Vertex>();
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous)
            path.add(vertex);
        Collections.reverse(path);
        return path;
    }

    public static void main(String[] args)
    {
	    Vertex vA = new Vertex("A");
		Vertex vB = new Vertex("B");
		Vertex vC = new Vertex("C");
		Vertex vD = new Vertex("D");
		Vertex vE = new Vertex("E");
		Vertex vF = new Vertex("F");
		Vertex vG = new Vertex("G");
		Vertex vH = new Vertex("H");
		
		vA.adjacencies = new Edge[]{ new Edge(vB, 24, 1) };//AB24(1)
		vB.adjacencies = new Edge[]{ new Edge(vA, 24, 1), new Edge(vC, 17, 1), new Edge(vF, 20, 2) };//BA24(a),BC17,BF20(1,2)
		vC.adjacencies = new Edge[]{ new Edge(vB, 17, 1), new Edge(vG, 17, 2), new Edge(vE, 23, 1), new Edge(vD, 25, 3)};//CB17(1),CG17(2),CE23(1),CD25(3)
		vD.adjacencies = new Edge[]{ new Edge(vC, 25, 3), new Edge(vE, 15, 2)  };//DC25(3),DE15(2)
		vE.adjacencies = new Edge[]{ new Edge(vD, 15, 2), new Edge(vC, 23, 1), new Edge(vH, 9,  3)  };//ED15(2),EC23(1),EH9(3)
		vF.adjacencies = new Edge[]{ new Edge(vB, 20, 2), new Edge(vG, 29, 2)  };//FB20(2),FG29(2)
		vG.adjacencies = new Edge[]{ new Edge(vC, 17, 2), new Edge(vF, 29, 2), new Edge(vH, 30, 3)  };//GF29(2),GC17(2),GH30(3)
		vH.adjacencies = new Edge[]{ new Edge(vG, 30, 3), new Edge(vE, 9,  3)  };//HG30(3),HE9(3)

		Vertex[] vertices = { vA, vB, vC, vD, vE, vF, vG, vH };
        computePaths(vA);
        for (Vertex v : vertices)
	{
	    System.out.println("Distance to: " + "("+ v +")" + ": " + v.minDistance + " km.");
	    List<Vertex> path = getShortestPathTo(v);
	    System.out.println("Path: " + path);
	    System.out.println("==================================");
	}
    }
}